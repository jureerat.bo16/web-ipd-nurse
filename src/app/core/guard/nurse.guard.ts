import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { NurseGuardService } from 'src/app/shared/guard/nurse-guard.service';

export const nurseGuard: CanActivateFn = (route, state) => {
  const authService = inject(NurseGuardService);
  return authService.isAllow();
};
