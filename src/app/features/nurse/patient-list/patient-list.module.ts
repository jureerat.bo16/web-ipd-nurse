import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientListRoutingModule } from './patient-list-routing.module';
import {PatientListComponent} from './patient-list.component';
import { MainComponent } from './main/main.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangeWardComponent } from './change-ward/change-ward.component';
import { NurseNoteComponent } from './nurse-note/nurse-note.component';
import { NgApexchartsModule } from "ng-apexcharts";


@NgModule({
  declarations: [
    PatientListComponent,MainComponent,
    ChangeWardComponent,
    NurseNoteComponent,
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,NgApexchartsModule,
    PatientListRoutingModule
  ]
})
export class PatientListModule { }
