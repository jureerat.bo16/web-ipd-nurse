import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NurseService } from '../services/nurse.service';
import { DateTime } from 'luxon';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../shared/services/lib.service';
import { UserProfileService } from '../../../core/services/user-profiles.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  dataWard: any;
  wardName: any;
  wards: any = [];
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  constructor(
    private router: Router,
    private nurseService: NurseService,
    private libService: LibService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService: UserProfileService
  ) {

  }

  async ngOnInit() {
    // this.user_login_name  =  this.userProfileService.user_login_name;  
    this.user_login_name = sessionStorage.getItem('userLoginName');

    console.log(this.user_login_name);
    this.getWard();
  }

  doSearch() {
    this.getAdmitActive();
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getAdmitActive()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getAdmitActive()
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getAdmitActive();
  }

  async onSelectWard(event: any) {
    console.log(event);

    this.dataWard = event;
    this.getAdmitActive();
    this.wardName = event.name;
  }

  async getAdmitActive() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.nurseService.getAdmitActive(this.dataWard.id, this.query, _limit, _offset);
      const data: any = response.data;
      // console.log(data);
      this.total = data.total || 1
      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
        v.admit_date = date;
        const time = v.admit_time ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat('HH:mm') : '';
        v.admit_time = time;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      if (!_.isEmpty(data)) {
        const selectedWard: any = data[0];
        this.dataWard = selectedWard;
        this.wardName = selectedWard.name;
      }
      await this.getAdmitActive();
    } catch (error: any) {
      console.log(error);
    }
  }

  async getWardName(wardId: any) {
    const rsWardName = await this.libService.getWardName(wardId);
    console.log(rsWardName.data.data[0]);
    return rsWardName.data.data[0];
  }

}
