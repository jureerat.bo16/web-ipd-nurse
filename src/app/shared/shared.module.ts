import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    FooterComponent,
  
  ],
  imports: [
    CommonModule,
    RouterModule,
    NzLayoutModule,
    NzMenuModule,
    NgxSpinnerModule,
   
  ],
  exports: [
    FooterComponent,
    
    NgxSpinnerModule
  ]
})
export class SharedModule { }
